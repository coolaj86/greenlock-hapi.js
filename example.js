'use strict';


//
// Configure Greenlock
//
var leConfDir = require('os').homedir() + '/letsencrypt/etc';

throw new Error(
    "You must edit the example to change the email address (and remove this error)."
  + " Also, you'll need to remove .testing() and rm -rf '" + leConfDir + "'"
  + " to get actual, trusted production certificates.");

var le = require('greenlock-express').create({
  server: 'staging' // in production use https://acme-v01.api.letsencrypt.org/directory

, configDir: require('os').homedir() + '/letsencrypt/etc'

, approveDomains: function (opts, certs, cb) {
    opts.domains = certs && certs.altnames || opts.domains;
    opts.email = 'john.doe@example.com' // CHANGE ME
    opts.agreeTos = true;

    cb(null, { options: opts, certs: certs });
  }

 , debug: true
});


//
// Be Hapi
//
var hapi = require('hapi');
var https = require('spdy');
var server = new hapi.Server();
var acmeResponder = le.middleware();
var httpsServer = https.createServer(le.httpsOptions).listen(443);

server.connection({ listener: httpsServer, autoListen: false, tls: true });

server.route({
  method: 'GET'
, path: '/.well-known/acme-challenge'
, handler: function (request, reply) {
    var req = request.raw.req;
    var res = request.raw.res;

    reply.close(false);
    acmeResponder(req, res);
  }
});

server.route({
  method: 'GET'
, path: '/'
, handler: function (request, reply) {
    reply("Hello, I'm so Hapi!");
  }
});


//
// Redirect HTTP to HTTPS
//
var http = require('http');
var redirectHttps = require('redirect-https')();

http.createServer(le.middleware(redirectHttps)).listen(80, function () {
  console.log('handle ACME http-01 challenge and redirect to https');
});
