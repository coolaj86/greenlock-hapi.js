# Greenlock&trade; for hapi

An Automated HTTPS ACME client (Let's Encrypt v2) for hapi

| Sponsered by [ppl](https://ppl.family)
| Greenlock&trade; is for
[Browsers](https://git.coolaj86.com/coolaj86/greenlock.html),
[Node.js](https://git.coolaj86.com/coolaj86/greenlock.js),
[Commandline](https://git.coolaj86.com/coolaj86/greenlock-cli.js),
[Express.js](https://git.coolaj86.com/coolaj86/greenlock-express.js),
[Node.js Cluster](https://git.coolaj86.com/coolaj86/greenlock-cluster.js),
**hapi**,
[Koa](https://git.coolaj86.com/coolaj86/greenlock-koa.js),
and [rill](https://git.coolaj86.com/coolaj86/greenlock-rill.js) |

Features
========

  * [x] Automatic Registration via SNI (`httpsOptions.SNICallback`)
  * [x] Secure domain approval callback
  * [x] Automatic renewal between 10 and 14 days before expiration
  * [x] Virtual Hosting (vhost) with Multiple Domains & SAN
  * [x] plugins for AWS, redis, etc
  * [x] and [more](https://git.coolaj86.com/coolaj86/greenlock-express.js)

This module is just an alias for greenlock-express.js,
which works with any middleware system.

Install
=======

```
npm install --save greenlock-hapi@2.x
```

QuickStart
==========

```javascript
'use strict';

//////////////////////
// Greenlock Setup  //
//////////////////////

var greenlock = require('greenlock-hapi').create({
  version: 'draft-11' // Let's Encrypt v2
  // You MUST change this to 'https://acme-v02.api.letsencrypt.org/directory' in production
, server: 'https://acme-staging-v02.api.letsencrypt.org/directory'

, email: 'jon@example.com'
, agreeTos: true
, approveDomains: [ 'example.com' ]

  // Join the community to get notified of important updates
  // and help make greenlock better
, communityMember: true

, configDir: require('os').homedir() + '/acme/etc'

//, debug: true
});


///////////////////
// Just add hapi //
///////////////////

var hapi = require('hapi');
var https = require('https');
var server = new hapi.Server();
var acmeResponder = greenlock.middleware();
var httpsServer = https.createServer(greenlock.httpsOptions).listen(443);

server.connection({ listener: httpsServer, autoListen: false, tls: true });

server.route({
  method: 'GET'
, path: '/.well-known/acme-challenge'
, handler: function (request, reply) {
    var req = request.raw.req;
    var res = request.raw.res;

    reply.close(false);
    acmeResponder(req, res);
  }
});

server.route({
  method: 'GET'
, path: '/'
, handler: function (request, reply) {
    reply("Hello, I'm so Hapi!");
  }
});


//
// http redirect to https
//
var http = require('http');
var redirectHttps = require('redirect-https')();

http.createServer(greenlock.middleware(redirectHttps)).listen(80, function () {
  console.log('Listening on port 80 to handle ACME http-01 challenge and redirect to https');
});
```

Usage & Troubleshooting
============================

See <https://git.coolaj86.com/coolaj86/greenlock-express.js>

Handling a dynamic list of domains
========================

In the oversimplified exapmple above we handle a static list of domains.
If you add domains programmatically you'll want to use the `approveDomains`
callback.

**SECURITY**: Be careful with this.
If you don't check that the domains being requested are the domains you
allow an attacker can make you hit your rate limit for failed verification
attempts.

We have a
[vhost example](https://git.coolaj86.com/coolaj86/greenlock-express.js/src/branch/master/examples/vhost.js)
that allows any domain for which there is a folder on the filesystem in a specific location.

See that example for an idea of how this is done.
