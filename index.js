'use strict';

module.exports = require('greenlock-express');
module.exports._greenlockExpressCreate = module.exports.create;
module.create = function (opts) {
  opts._communityPackage = opts._communityPackage || 'greenlock-hapi';
  return module.exports._greenlockExpressCreate(opts);
};
